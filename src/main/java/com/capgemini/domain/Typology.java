package com.capgemini.domain;

public enum Typology {
    FLAT,
    CHALET,
    GARAGE,
}
