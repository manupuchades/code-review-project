package com.capgemini.application;

import com.capgemini.infrastructure.api.PublicAd;
import com.capgemini.infrastructure.api.QualityAd;

import java.util.List;

public interface AdsService {

    List<PublicAd> findPublicAds();
    List<QualityAd> findQualityAds();
    void calculateScores();
}
